//If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
//
//Find the sum of all the multiples of 3 or 5 below 1000.


pub fn mul_3_5_sum(limit: i32) -> i32 {
    let mut sum = 0;

    for num in 1..limit {
        if (num % 3 == 0) || (num % 5 == 0) {
            sum = sum + num;
        }
    }
    return sum;
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_max_10() {
        assert_eq!(23, mul_3_5_sum(10));
    }

    #[test]
    fn test_max_1000() {
        assert_eq!(233168, mul_3_5_sum(1000));
    }
}