#!/usr/bin/env ruby


# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
#
# Find the sum of all the primes below two million.
require 'mathn'



## ruby's prime number generator is painfully slow on 1.8.7
#sum = Prime.new.inject(0) { |sum, n| 
#  puts "n #{n} sum #{sum}"
#  break sum unless n < 2_000_000
#  sum + n
#}
#p "sum of primes: #{sum}"
#



# this can be optimized a few ways.
# prime calculation is not efficient
# could save the old prime numbers in an array and only divide by them

prime_count = 0
current_num = 3

sum_of_primes = 2

#while(current_num < 10)
while(current_num < 2000000)

   prime = 1

   2.upto(Math.sqrt(current_num) + 1) do |x|

      if( (prime == 1) && (current_num % x) == 0)
         #puts " #{current_num} #{x} is NOT prime" 
         prime = 0
      end

   end

   if (1 == prime)
      prime_count = prime_count + 1
      sum_of_primes = sum_of_primes + current_num
      puts " #{current_num} is prime, (found #{prime_count}), sum: #{sum_of_primes}" 
   end
   
   #skip evens
   current_num = current_num + 2
end

puts "#{sum_of_primes} "

# answer: 142 913 828 922

