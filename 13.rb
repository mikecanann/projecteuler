#!/usr/bin/env ruby


# Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.


sum = 0

File.readlines("13.txt").each do |line|
 
  sum = sum + line.to_i
  puts sum
end

# answer is the first 10 digits of the last line printed out



