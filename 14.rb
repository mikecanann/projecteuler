#!/usr/bin/env ruby


longest_chain = 0
starting_number = 0

#x = 13

999999.downto(1) do |x|

   chain = 1

   n = x
   while (n > 1)

      if ((n % 2) == 0)
         n = n / 2 
      else
         n = (3 * n) + 1 
      end

      chain = chain + 1

      if(chain > longest_chain)
         longest_chain = chain
         starting_number = x
      end

      #puts n
   end
end


puts "the longest chain:"
puts longest_chain
puts "the starting number:"
puts starting_number
