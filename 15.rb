#!/usr/bin/env ruby

#bimonial coefficients
#  40!  / (20! * 20!)


puts ((1..40).inject(:*) / ((1..20).inject(:*) * (1..20).inject(:*)) )
