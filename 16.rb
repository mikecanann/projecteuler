#!/usr/bin/env ruby



#2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
#
#What is the sum of the digits of the number 2^1000?

#digits =  2 ** 15
digits =  2 ** 1000


sum_of_digits = 0

digits.to_s().split("").each{|test|
   sum_of_digits = sum_of_digits + test.to_i
}

puts sum_of_digits

# 1366
