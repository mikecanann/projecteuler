#!/usr/bin/env ruby


require 'rubygems'
require 'linguistics' # gem install linguistics
Linguistics::use( :en ) # extends Array, String, and Numeric


length = 0

#(1..5).each{|num|
(1..1000).each{|num|
   puts num.en.numwords

   text = num.en.numwords

   # remove space and dash
   text = text.gsub(/[ -]/, '');

   puts text
   length = length + text.length 
}

#24527 (with spaces and dash)
#21124 (no space or dash 
puts length

