#!/usr/bin/env ruby

# n! means n × (n − 1) × ... × 3 × 2 × 1
#
# For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
# and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
#
# Find the sum of the digits in the number 100!


#start_number = 10 # simple example
start_number = 100 # real data

# get the factorial 
# convert to string - breaking into an array of char
# convert the char to int and sum them
sum = (1..start_number).inject(:*).to_s().split("").inject(0){|sum, n| sum + n.to_i} 

puts "answer = #{sum}"

# 648
