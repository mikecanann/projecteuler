#!/usr/bin/env ruby

#Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.
#
#For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.
#
#What is the total of all the name scores in the file?



names = []


File.readlines("22.txt").each do |line|
  names = line.split(",")
end

names.sort!


total_score = 0
count = 1
names.each{|cur_name|

   striped_name = cur_name.gsub("\"", "");
   puts "#{count} #{striped_name}"


   letters = striped_name.split("")

   word_count = 0
   letters.each{|letter|
      print "#{letter} ";
      puts (letter[0] - 64)
      word_count += (letter[0] - 64)
   }
   puts "word count #{word_count} "

   total_score += (word_count * count)

   # debug code to display the sample from the instructions
   #if(count > 938)
   #   exit 
   #end

   count += 1
}

puts "total score = #{total_score}"

# 871198282
