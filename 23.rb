#!/usr/bin/env ruby


#A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.
#
#A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.
#
#As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.
#
#Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.

require 'my_math'


sums = []

abundants = (1..28213).select { |num| is_abundant?(num) }

#calculate all sums of 2 abundant numbers up to 28124
abundants.each_with_index do |ab_num, x|
	(x .. abundants.length - 1).each do |y|
		sum = ab_num + abundants[y]
		sums << sum unless sum > 28213
	end
end

sums.uniq!
puts (1..28213).reject { |num| sums.include? num }.reduce(:+)

# 4179871

