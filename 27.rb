#!/usr/bin/env ruby

#Euler published the remarkable quadratic formula:
#
#n² + n + 41
#
#It turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39. However, when n = 40, 402 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, and certainly when n = 41, 41² + 41 + 41 is clearly divisible by 41.
#
#Using computers, the incredible formula  n² − 79n + 1601 was discovered, which produces 80 primes for the consecutive values n = 0 to 79. The product of the coefficients, −79 and 1601, is −126479.
#
#Considering quadratics of the form:
#
#    n² + an + b, where |a| < 1000 and |b| < 1000
#
#    where |n| is the modulus/absolute value of n
#    e.g. |11| = 11 and |−4| = 4
#
#Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n = 0.
#


require 'my_math'


def quadratic_formula(n, a, b)
  (n * n) + (a * n) + b
end

max = 0
best_a = 0
best_b = 0

(-999..999).step(2).to_a.permutation(2) do |a, b|
   consec = 0
   n = -1
   while(quadratic_formula(n += 1, a, b).is_prime?)
      consec += 1 
   end
   if(consec > max)
      max = consec
      best_a = a
      best_b = b
  end
end

puts "a: #{best_a}, b: #{best_b}"
puts "answer #{best_a * best_b}"
