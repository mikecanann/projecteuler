#!/usr/bin/env ruby
#Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:
#
#21 22 23 24 25
#20  7  8  9 10
#19  6  1  2 11
#18  5  4  3 12
#17 16 15 14 13
#
#It can be verified that the sum of the numbers on the diagonals is 101.
#
#What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
#







# step will start as 2 (for 4 elements) and increase by 2 each time around

# 73 74 75 76 77 78 79 80 81
# 72 43 44 45 46 47 48 49 50
# 71 42 21 22 23 24 25 26 51
# 70 41 20  7  8  9 10 27 52
# 69 40 19  6  1  2 11 28 53
# 68 39 18  5  4  3 12 29 54
# 67 38 17 16 15 14 13 30 55
# 66 37 36 35 34 33 32 31 56
# 65 64 63 62 61 60 59 58 57



i = 1
sum = i
step = 2
max_i = 5 * 5 # test value
max_i = 1001 * 1001
until i >= max_i
	(1..4).each { 
      i += step 
      sum += i 
      puts "i #{i} sum #{sum} step #{step}"
   }
	step += 2
end
puts sum

