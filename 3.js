#!/usr/bin/env node
"use strict";


// The prime factors of 13195 are 5, 7, 13 and 29.
//
// What is the largest prime factor of the number 600851475143 ?

// 6857


(function () {

  //var num = 13195 // sample value

  var num = 600851475143;

  var root;
  var factors = [];
  var x;
  var doLoop = true;
  while (doLoop) {
    root = Math.sqrt(num);
    x = 2;
    if (num % x) {
      x = 3;
      while ((num % x) && ((x += 2) < root));
    }
    if(x > root) {
      x = num;
    }
    factors.push(x);
    doLoop = ( x != num );
    num = num / x;
  }

  // print all factors
  console.log(factors);

  // print the largest prime
  console.log(Math.max.apply(null, factors));


}());


