#!/usr/bin/env ruby

# The prime factors of 13195 are 5, 7, 13 and 29.
#
# What is the largest prime factor of the number 600851475143 ?




# I started out doing this the hard way.
# ruby comes with prime_division function in mathn. 
require 'mathn'

target = 13195 # test value
target = 600851475143


puts target.prime_division.last[0]

