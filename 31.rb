#!/usr/bioon/env ruby

# In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:
#
#    1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
#
#It is possible to make £2 in the following way:
#
#    1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
#
#How many different ways can £2 be made using any number of coins?



ways = 0
200.step(0, -200) { |a|
   a.step(0, -100) { |b|
      b.step(0, -50) { |c|
         c.step(0, -20) { |d|
            d.step(0, -10) { |e|
               e.step(0, -5) { |f|
                  f.step(0, -2) { |g|
                     #puts "#{a} #{b} #{c} #{d} #{e} #{f} #{g}"
                     ways += 1
                  }
               }
            }
         }
      }
   }
}
puts ways
