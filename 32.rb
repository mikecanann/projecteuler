#!/usr/bin/env ruby

# We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.
#
#The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.
#
#Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
#HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.
#


results = Array.new

def pandigital?(iden)
   if((iden.length == 9) &&
      (iden.split(//).sort.join == '123456789'))
      return(true)
   end
   return(false)
end

2.upto(5000) do |i|
   i.upto(9999 / i) do |j|
      if pandigital?(i.to_s + j.to_s + (i * j).to_s)
         results << (i * j)
      end
   end
end

puts results.uniq.inject(0){|sum, k| sum + k}

# answer 45228

