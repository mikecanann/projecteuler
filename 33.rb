#!/usr/bin/env ruby



# The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.
#
#We shall consider fractions like, 30/50 = 3/5, to be trivial examples.
#
#There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.
#
#If the product of these four fractions is given in its lowest common terms, find the value of the denominator.

#----
# The problem could be worded better looking for all the cases where:
#    ab/bc = a/c  
# and 
#    a != b, a != c, b != c

# 4 curios fractions:
#16/64 19/95 26/65 49/98

# product: 8/800 - lowest common term: 1/100 
# answer: 100


nProd = 1
dProd = 1
(10..99).to_a.product((10..99).to_a).select { |num, den|

   #  (49 != 98  &&       49/98 == (49/10)     /(98%10) && (49%10) == (98/10))
   #  (49 != 98  &&       49/98 == (4)         / (8)    && (9)     == (9))
   #  (49 != 98  &&       0.5   ==         0.5          && (9)     == (9))
   if(num != den && num*1.0/den == (num/10)*1.0/(den%10) && (num%10) == (den/10))
      puts "cf: #{num}/#{den}"
      nProd *= (num/10)
      dProd *= (den%10)
   end
}
puts "#{nProd} / #{dProd} "
puts "#{nProd/8} / #{dProd/8} "

