#!/usr/bin/env ruby

#145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
#
#Find the sum of all numbers which are equal to the sum of the factorial of their digits.
#
#Note: as 1! = 1 and 2! = 2 are not sums they are not included.
#
#


# zero factorial = 1.  so any number with zero will not work

def factorial(val)
   (1..val).reduce(1, :*)
end

puts (10..50000).select { |i|
	i == (i.to_s.each_char.map { |d| factorial(d.to_i) }.reduce(:+))
}.reduce(:+)

# answer: 40730
