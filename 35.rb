#!/usr/bin/env ruby

#
#
# The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
#
#There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
#
#How many circular primes are there below one million?


#require 'mathn'

# no rotate in ruby 1.8, copying the code from 1.9
class Array
  def rotate n = 1; self[n..-1]+self[0...n] end
end

# libprime.rb - from projecteuler.net
# A much faster prime class.
#
class Integer
  def isPrime?
    if self <2
      return false
    elsif self < 4
      return true
    elsif self % 2 == 0
      return false
    elsif self < 9
      return true
    elsif self % 3 == 0
      return false
    else
      r = (self**0.5).floor
      f = 5
      f.step r,6 do |g|
        if self % g == 0
          return false
        end
        if self % (g + 2) == 0
          return false
        end
      end
      return true
    end
  end
end

class Prime
  def initialize
    @last_prime = nil
  end
 
  def succ
    if @last_prime.nil?
      @last_prime = 2
      return 2
    else
      i = @last_prime + 1
      i += 1 if i % 2 == 0
      while not i.isPrime?
        i += 2
      end
      @last_prime = i
      return i
    end
  end
  alias next succ
  def each
    loop do
      yield succ
    end
  end
end




puts (1..1000000).select { |i|
	(1..i.to_s.length).all? { |j|
		i.to_s.split('').rotate(j).join('').to_i.isPrime?
	}
}.count


# answer: 55
