#!/usr/bin/env ruby



# The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.
#
#Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
#
#(Please note that the palindromic number, in either base, may not include leading zeros.)
#



puts (1..1000000).select { |i|
   (i.to_s == i.to_s.reverse) && (i.to_s(2) == i.to_s(2).reverse) 
}.reduce(:+)


# answer: 872187


