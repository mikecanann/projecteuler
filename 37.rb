#!/usr/bin/env ruby

# 
#
# The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.
#
# Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
#
# NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
#



#require 'mathn'


# libprime.rb - from projecteuler.net
# A much faster prime class.
#
class Integer
  def isPrime?
    if self <2
      return false
    elsif self < 4
      return true
    elsif self % 2 == 0
      return false
    elsif self < 9
      return true
    elsif self % 3 == 0
      return false
    else
      r = (self**0.5).floor
      f = 5
      f.step r,6 do |g|
        if self % g == 0
          return false
        end
        if self % (g + 2) == 0
          return false
        end
      end
      return true
    end
  end
end

class Prime
  def initialize
    @last_prime = nil
  end
 
  def succ
    if @last_prime.nil?
      @last_prime = 2
      return 2
    else
      i = @last_prime + 1
      i += 1 if i % 2 == 0
      while not i.isPrime?
        i += 2
      end
      @last_prime = i
      return i
    end
  end
  alias next succ
  def each
    loop do
      yield succ
    end
  end
end



puts (10..1000000).select { |i|
	(0..i.to_s.length-1).all? { |j|
		i.to_s[0..j].to_i.isPrime? && i.to_s[j..-1].to_i.isPrime?
	}
}.reduce(:+)



# answer: 748317
