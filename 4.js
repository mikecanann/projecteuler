#!/usr/bin/env node

// A palindromic number reads the same both ways.
// The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
//
// Find the largest palindrome made from the product of two 3-digit numbers.

largest = 1

for (let x = 100; x <= 999; x++) {
  for (let y = 100; y <= 999; y++) {


    let sum =  x * y;
    let sum_string = `${sum}`
    // are they palindromic
    if(sum_string == sum_string.split("").reverse().join("")) {
      if(sum > largest) {
        largest = sum
        // console.log(largest)
      }
    }
  }
}


console.log(`largest: ${largest}`)


