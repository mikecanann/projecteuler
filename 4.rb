#!/usr/bin/env ruby

#A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
#
#Find the largest palindrome made from the product of two 3-digit numbers.

largest = 1

100.upto(999) do |x|

   100.upto(999) do |y|

      sum =  x * y 
      
      sum_string = "#{sum}"
      # are they palindromic
      if(sum_string == sum_string.reverse)
         if(sum > largest)
            largest = sum
            #puts largest
         end
      end

   end
end


puts "largest: #{largest}"


