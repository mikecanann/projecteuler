#!/usr/bin/env node


// 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
//
// What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
//



/**
 * Calculate lcm for two numbers
 * @param {number} a
 * @param {number} b
 * @returns {number} Returns the least common multiple of a and b
 */
function lcmNumber (a, b) {
  if (!Number.isInteger(a) || !Number.isInteger(b)) {
    throw new Error('Parameters in function lcm must be integer numbers');
  }

  if (a === 0 || b === 0) {
    return 0;
  }

  // https://en.wikipedia.org/wiki/Euclidean_algorithm
  // evaluate lcm here inline to reduce overhead
  let t;
  const prod = a * b;
  while (b !== 0) {
    t = b;
    b = a % t;
    a = t;
  }
  return Math.abs(prod / a);
}

let lowerbound = 1;
let upperbound = 20;
range = Array.from(new Array(upperbound), (x, i) => i + lowerbound);

let result = 0;
reducer = (accumulator, currentValue) => (lcmNumber(accumulator, currentValue));
result = range.reduce(reducer, 1);

console.log(result);
// 232792560

