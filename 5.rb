#!/usr/bin/env ruby


# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
#
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
#

# require 'rational'

puts((1..20).reduce(1) { |result, element|
  # puts "result #{result}"
  # puts "element #{element}"
  result.lcm(element)
})
