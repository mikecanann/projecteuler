#!/usr/bin/env ruby


#By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
#
#3
#7 4
#2 4 6
#8 5 9 3
#
#That is, 3 + 7 + 4 + 9 = 23.
#
#Find the maximum total from top to bottom in triangle.txt (right click and 'Save Link/Target As...'), a 15K text file containing a triangle with one-hundred rows.
#
#NOTE: This is a much more difficult version of Problem 18. It is not possible to try every route to solve this problem, as there are 2^99 altogether! If you could check one trillion (1012) routes every second it would take over twenty billion years to check them all. There is an efficient algorithm to solve it. ;o)




require 'rubygems'



# solve by starting at the bottom row, and adding the highest value to the row above it:
#
# 1)
#     3
#    7 4
#   2 4 6
#  8 5 9 3
#
# 2) 
#       3
#      7 4
#   10 13 15
#     
# 3)
#       3
#      20 19
#  
# 4) 
#       23


triangle = []

input_row = 0

File.readlines("67.txt").each do |line|
 
  #puts line
  triangle[input_row] = line.split(" ")
  input_row = input_row + 1
end

triangle.reverse!



(0..(input_row - 1)).each{|row|

   print "row #{row}: "
   (0..(triangle[row].size - 1)).each{|col|
      print triangle[row][col]
      print " "

      next if(col > (triangle[row].size - 2))

      if(triangle[row][col].to_i > triangle[row][col + 1].to_i)
         triangle[row + 1][col] = triangle[row + 1][col].to_i + triangle[row][col].to_i 
      else
         triangle[row + 1][col] = triangle[row + 1][col].to_i + triangle[row][col + 1].to_i 
      end
   }
   puts " "

   
   
   


}




#row 0: 04  62  98  27 23  09 70  98  73  93  38  53   60 04 23  
#row 1:  125 164 102  95 112 123  165 128 166 106 122 147 100 54  
#row 2:    255 235 154 150 140 179 256 171  224 172 174 176 148  
#row 3: 70 11 33 28 77 73 17 78 39 68 17 57  
#row 4: 53 71 44 65 25 43 91 52 97 51 14  
#row 5: 41 48 72 33 47 32 37 16 94 29  
#row 6: 41 41 26 56 83 40 80 70 33  
#row 7: 99 65 04 28 06 16 70 92  
#row 8: 88 02 77 73 07 63 67  
#row 9: 19 01 23 75 03 34  
#row 10: 20 04 82 47 65  
#row 11: 18 35 87 10  
#row 12: 17 47 82  
#row 13: 95 64  
#row 14: 75  

