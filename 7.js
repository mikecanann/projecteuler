


const primes = [ 2, 3 ];
function isPrime(n) {
  return primes.every(d => n % d > 0);
}

let i = 1;
while (primes.length < 10001) {
  const n = i * 3 + 1.5 + .5 * (-1) ** (i + 1);
  if (isPrime(n)) primes.push(n);
  i++;
}
console.log(primes.length)
console.log(primes[10000]); 
console.log(primes[primes.length - 1]);
