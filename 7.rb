#!/usr/bin/env ruby

# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
#
# What is the 10 001st prime number?


require 'mathn'


answer = 0
count = 1
Prime.each(200_000) do |prime|
  if count == 10_001
    answer = prime
  end
  count += 1
end
p answer

