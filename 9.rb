#!/usr/bin/env ruby

# A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
# a^2 + b^2 = c^2
#
# For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
#
# There exists exactly one Pythagorean triplet for which a + b + c = 1000.
# Find the product abc.




# brute force solution - takes minutes to run
# need to come back to this one and optimize,
# there must be an algorithm to solve Pythagorean triplets

1.upto(999) do |x|
   1.upto(999) do |y|
      1.upto(999) do |z|
         
         if(1000 ==  (x + y + z))
            if((x * x + y * y) == (z * z))
               puts "answer: #{x} #{y} #{z}"
               exit
            end
         end

      end
   end
end


