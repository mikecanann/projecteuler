

# return a list of divisors of num
# used in 23 
def divisors(num)
  max = Math.sqrt(num).floor
  divisors=[]
  1.upto(max) do |i|
	if (num % i == 0)
	  divisors.push(i)
	  if((num / i != i) and (num/i!=num))
	     divisors.push(num / i) 
     end
	end
  end
  divisors

end

# used in 23 
def sum_divisors(divisors)
  sum=0
  divisors.each{|num| sum += num}
  sum
end

# used in 23 
def is_abundant?(num)
  sum_divisors(divisors(num)) > num
end


# claculate the fibonacci value, with caching
# used in 25
def fibonacci_c(n)

   # create the cache if it doesn't exits
   @fib_cache ||= Array.new   

   return @fib_cache[n] if @fib_cache[n] 
   return 1 if n <= 2
   @fib_cache[n] = fibonacci_c(n - 1) + fibonacci_c(n - 2)
end

# returns the first fibonacci number that contains terms digits 
# used in 25
def fibonacci_terms(terms)
   curr = 0
   while(fibonacci_c(curr).to_s.length < terms)
      curr += 1
   end
   return curr
end

